import {store} from '@angular/core/src/render3/instructions';

declare const require: any;
import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import * as ol from 'openlayers';
import leaflet from 'leaflet';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import * as firebase from 'firebase/app';
require('firebase/database');
require('graphhopper-js-api-client');
const GraphHopperRouting = require('graphhopper-js-api-client/src/GraphHopperRouting');
const GHInput = require('graphhopper-js-api-client/src/GHInput');
const config = {
  apiKey: 'f1ec60fb-d2a1-43e1-b40f-89f213a71dcd',
  authDomain: 'amora-2cc4c.firebaseapp.com',
  databaseURL: 'https://amora-2cc4c.firebaseio.com',
  projectId: 'amora-2cc4c',
  storageBucket: 'amora-2cc4c.appspot.com',
  messagingSenderId: '330634973318'
};
firebase.initializeApp(config);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  markers = [];
  provider = new OpenStreetMapProvider();
  searchControl;
  defaultKey = '758ed79b-b279-4d52-9696-ba6fedcbe290';
  profile = 'car';
  ghRouting;
  latitude: number;
  longitude: number;
  searchEvent;
  mymap;
  clickable = true;
  coordinates;
  marker;
  newPolyline;
  directions = [];
  direction;
  pointPairs = [];
  num = 1;
  searchedMarker;
  searched = false;
  vehicle;
  currentLocation;
  reset = false;
  recentRoute;
  routeName;
  setRoute;
  routeList;
  routes = [];
  historyObject;
  db = firebase.database();
  constructor (private ngZone: NgZone) {
    window['app-root'] = {component: this, zone: ngZone};
  }
  ngOnInit() {
    console.log('GraphHopper?', GraphHopperRouting);
    this.mymap = leaflet.map('mapid').setView([40.286316, -111.686715], 13);
    leaflet.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}' +
      '.png?access_token=pk.eyJ1IjoiYmVuYXRtaWNyb2V4Y2VsIiwiYSI6ImNqbGZzNHRtNzEwNGszcGxxNXI1Nm1jdWkifQ.OcG3ibnmNo4Vt1rDlgGA8w', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'your.mapbox.access.token'
    }).addTo(this.mymap);
    this.searchControl = new GeoSearchControl({
      provider: this.provider,
      autoComplete: true,
      autoCompleteDelay: 200,
      marker: {
        draggable: false,
        icon: new leaflet.DivIcon({
          html: '<span>' + this.num + '</span>'
        })
      }
    });
    this.mymap.on('dblclick', <LeafletMouseEvent>(e) => {
      console.log(e.latlng);
      this.clickable = true;
      this.updateLatLng(e.latlng);
    });
    this.mymap.on('click', <LeafletMouseEvent>(e) => {
      console.log(e.latlng);
      this.clickable = true;
      this.removeMarker(e.latlng);
    });
    this.mymap.doubleClickZoom.disable();
    this.mymap.addControl(this.searchControl);
    this.mymap.on('geosearch/showlocation', <LeafletMouseEvent>(e) => {
      this.addPoint2(e);
    });
    this.ghRouting = new GraphHopperRouting({
      key: this.defaultKey,
      vehicle: this.profile,
      elevation: false
    });
    this.reuseRecentRoute2();
  }
  ngOnDestroy() {
    window['app-root'].component = null;
  }
  addPoint2(e) {
    this.searched = true;
    console.log('Added...', e.marker._latlng);
    this.num = this.num + 1;
    this.searchedMarker = e.marker;
    let options = {
      title: this.num = this.markers.length + 1,
      draggable: false,
      icon: new leaflet.DivIcon({
        html: '<span>' + this.num + '</span>'
      })
    };
    console.log('Map contents', this.mymap._targets);
    this.marker = leaflet.marker([e.marker._latlng.lat, e.marker._latlng.lng], options);
    this.marker.addTo(this.mymap);
    this.markers.push(this.marker);
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].on('click', <LeafletMouseEvent>(e) => {
        console.log('Location', e);
        this.removeMarker(e);
      });
    }
  }
  locationFound(e) {
    console.log('Current location is...', e);
    console.log('Vehicle', this.vehicle);
    let options = {
      draggable: false,
      icon: new leaflet.DivIcon({
        html: '<span>Vehicle</span>'
      })
    };
      if (this.vehicle) {
        console.log('Vehicle', this.vehicle);
      this.resetVehicle();
        this.vehicle = leaflet.marker([e[0], e[1]], options);
        this.vehicle.addTo(this.mymap);
      } else {
        this.vehicle = leaflet.marker([e[0], e[1]], options);
        this.vehicle.addTo(this.mymap);
      }
    console.log('Got past marker', this.mymap);
  }
  addVehicle(e) {
    //
  }
  sleep(milliseconds) {
    const start = new Date().getTime();
    for (let i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds) {
        break;
      }
    }
  }
  startFollow() {
    this.currentLocation = this.db.ref('location');
    console.log('Current location', this.currentLocation);
    this.currentLocation.on('value', function(snapshot) {
      console.log(window['app-root'].zone);
        window['app-root'].zone.run(() => {
          window['app-root'].component.locationFound(snapshot.val());
        });
    });
  }
  reuseRecentRoute() {
    this.recentRoute = this.db.ref('/route/');
      this.recentRoute.on('value', function(snapshot) {
        window['app-root'].zone.run(() => {
          window['app-root'].component.recycleRoute(snapshot.val());
        });
      });
  }
  reuseRecentRoute2() {
    this.recentRoute = this.db.ref('/routes/');
    this.recentRoute.on('value', function(snapshot) {
      window['app-root'].zone.run(() => {
        window['app-root'].component.recycleRoute2(snapshot.val());
      });
    });
  }
  recycleRoute(route) {
    if (route) {
      console.log(route[0]['snapped_waypoints']);
      let routeCoords = route[0]['snapped_waypoints']['coordinates'];
      console.log('Any length here?', routeCoords, routeCoords.length);
      for (let i = 0; i < routeCoords.length; i++) {
        this.updateLatLng2(routeCoords[i]);
      }
      this.sendCoordinates();
    }
  }
  recycleRoute2(route) {
    if (route) {
      // console.log('Length', Object.keys(route).length);
      for (let key in route) {
        if (!route.hasOwnProperty(key)) continue;
        this.historyObject = route[key];
        console.log('Object acquired', this.historyObject, route);
        this.sortRoute(route);
      }
    }
  }
  sortRoute(route) {
      let listedRoute = { name: this.historyObject[this.historyObject.length - 1], coordinate: this.historyObject };
      console.log('Named route', listedRoute);
      this.routes.push(listedRoute);
      console.log('Listed route', this.routes);
  }
  selectRoute(e) {
    console.log('Event value', e);
    let event = e.match(/[^,]+,[^,]+/g);
    console.log('Event', event);
    for (let i = 0; i < event.length; i++) {
      let eventSplit = event[i].split(',');
      this.updateLatLng2(eventSplit);
      console.log('The event split', eventSplit);
    }
  }
  resetVehicle() {
    this.reset = true;
    if (this.reset) {
      if (this.mymap._targets) {
        let mapTarget = this.mymap._targets;
        console.log('Map targets', mapTarget);
        for (let key in mapTarget) {
          console.log('loop entered');
          if (!mapTarget.hasOwnProperty(key)) continue;
          let storedMarker = mapTarget[key];
          if (storedMarker === this.vehicle) {
            this.mymap.removeLayer(storedMarker);
            this.reset = false;
            this.vehicle = '';
          }
        }
      }
    }
  }
  addPoint() {
    this.searched = false;
    console.log('Added...', this.searchEvent);
    this.marker.addTo(this.mymap);
    this.num = this.num + 1;
    this.markers.push(this.marker);
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].on('click', <LeafletMouseEvent>(e) => {
        console.log('Location', e);
        this.removeMarker(e);
      });
    }
  }
  addRouteToList() {
    // let routeObject = {};
    console.log('Saving route...', this.setRoute);
    this.setRoute.push(this.routeName);
    if (this.setRoute) {
      this.db.ref('routes/').push(this.setRoute)
        .then((data) => {
          console.log('Sent to database?', data);
          return data;
        });
    }
      // let revisedRouteName = this.routeName.replace(/ /g, '-').toLowerCase();
      // console.log('Compatible', revisedRouteName);
      // for (let i = 0; i < this.setRoute.length; i++) {
      //   routeObject[i] = this.setRoute[i];
      // }
  }
  retrieveSavedRoutes() {
    this.routeList = this.db.ref('/routes/');
    this.routeList.on('value', function(snapshot) {
      window['app-root'].zone.run(() => {
        window['app-root'].component.recycleRoute(snapshot.val());
      });
    });
  }
  updateLatLng(event) {
    console.log('Update', this.mymap);
    if (event.lat) {
      if (this.marker) {
        console.log(this.marker);
        this.latitude = event.lat;
        this.longitude = event.lng;
        let options = {
          title: this.num = this.markers.length + 1,
          draggable: false,
          icon: new leaflet.DivIcon({
            html: '<span>' + this.num + '</span>'
          })
        };
        this.marker = leaflet.marker([this.latitude, this.longitude], options);
        console.log(this.marker);
        this.addPoint();
      } else {
        console.log(event);
        this.latitude = event.lat;
        this.longitude = event.lng;
        let options = {
          title: this.num = this.markers.length + 1,
          draggable: false,
          icon: new leaflet.DivIcon({
            html: '<span>' + this.num + '</span>'
          })
        };
        this.marker = leaflet.marker([this.latitude, this.longitude], options);
        this.marker.addTo(this.mymap);
        console.log(this.marker);
        this.addPoint();
      }
    }
  }
  updateLatLng2(event) {
    console.log('Event values', typeof(event));
    if (event[0]) {
      if (this.marker) {
        console.log(this.marker);
        // this.softClear();
        this.latitude = event[1];
        this.longitude = event[0];
        let options = {
          title: this.num = this.markers.length + 1,
          draggable: false,
          icon: new leaflet.DivIcon({
            html: '<span>' + this.num + '</span>'
          })
        };
        this.marker = leaflet.marker([this.latitude, this.longitude], options);
        console.log(this.marker);
        this.addPoint();
      } else {
        console.log(event);
        this.latitude = event[1];
        this.longitude = event[0];
        let options = {
          title: this.num = this.markers.length + 1,
          draggable: false,
          icon: new leaflet.DivIcon({
            html: '<span>' + this.num + '</span>'
          })
        };
        this.marker = leaflet.marker([this.latitude, this.longitude], options);
        this.marker.addTo(this.mymap);
        console.log(this.marker);
        this.addPoint();
      }
    }
  }
  generatePath(graphHopper) {
    let pointList = graphHopper.paths[0].points.coordinates;
    for (let i = 0; i < pointList.length; i++) {
      let lat = pointList[i][1];
      let long = pointList[i][0];
      let pointPair = new leaflet.LatLng(lat, long);
      this.pointPairs.push(pointPair);
    }
    this.newPolyline = new leaflet.Polyline(this.pointPairs, {
      color: 'red',
      weight: 2,
      opacity: 0.5,
      smoothFactor: 1
    });
    this.newPolyline.addTo(this.mymap);
    let paths = graphHopper.paths[0].instructions;
    console.log('Paths are', paths);
    for (let x = 0; x < paths.length; x++) {
      this.direction = paths[x];
      this.directions.push(this.direction);
    }
  }
  removeMarker(e) {
    console.log('function entered', this.mymap);
    for (let i = 0; i < this.markers.length; i++) {
      console.log('Marker', e.target, this.markers[i]);
      if (e.target === this.markers[i]) {
        this.mymap.removeLayer(this.markers[i]);
        this.markers.splice(i, 1);
      }
    }
    if (this.searched) {
      if (this.mymap._targets) {
        let mapTarget = this.mymap._targets;
        console.log('Map targets', mapTarget);
        for (let key in mapTarget) {
          console.log('loop entered');
          if (!mapTarget.hasOwnProperty(key)) continue;
          let storedMarker = mapTarget[key];
          console.log('contained markers', storedMarker, e.target._latlng);
          for (let x = 0; x < this.markers.length; x++) {
            if (e.target === storedMarker) {
              console.log('Reached', storedMarker);
              this.mymap.removeLayer(storedMarker);
              if (this.markers[x] === storedMarker) {
                this.markers.splice(x, 1);
              }
            }
          }
        }
      }
    }
  }
  removeMarkers() {
    for (let i = 0; i < this.markers.length; i++) {
      this.mymap.removeLayer(this.markers[i]);
    }
    this.latitude = null;
    this.longitude = null;
    this.markers = [];
    this.pointPairs = [];
    this.mymap.removeLayer(this.newPolyline);
    this.ghRouting.points = [];
    this.directions = [];
    this.direction = '';
    console.log('Map contains', this.ghRouting);
  }

  sendCoordinates() {
    if (this.markers.length < 2) {
      console.log('At least 2 lat/long sets needed');
    } else {
      for (let i = 0; i < this.markers.length; i++) {
        this.ghRouting.addPoint(new GHInput(this.markers[i]._latlng));
      }
      this.ghRouting.doRequest().then((result) => {
        if (result) {
          this.generatePath(result);
          console.log('Is this changing?', result['paths'][0]['snapped_waypoints']['coordinates']);
          this.setRoute = result['paths'][0]['snapped_waypoints']['coordinates'];
          if (this.routeName) {
            this.addRouteToList();
          }
          this.db.ref('route/').set(result['paths'])
            .then((data) => {
            console.log('Sent to database?', data);
            return data;
          });
        }
      }).catch((error) => {
        console.log(error);
      });
    }
  }
}
